class BaseHandler {

  constructor(logic) {
    this.logic = logic;
  }

  getToken(authorization) {
    return JSON.parse(decodeURIComponent(new Buffer(authorization.split('.')[1], 'base64').toString('binary')));
  }

  doRequest(f) {
    return (req, res) => {
      try {
        f(req, res);
      } catch (e) {
        console.error(e);
        res.status(500).json({});
      }
    };
  }

}

module.exports = BaseHandler;
