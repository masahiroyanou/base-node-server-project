const BaseHandler = require('./base');

class AuthHandler extends BaseHandler {

  constructor(logic) {
    super(logic);
  }

  doLogin() {
    return this.doRequest(async (req, res) => {
      const { email, password } = req.body;
      // TODO validate request body

      const result = await this.logic.auth.doLogin(email, password);
      if (!result) {
        res.status(401).json({});
      } else {
        res.json(result);
      }
    });
  }

}

module.exports = AuthHandler;
