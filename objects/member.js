const moment = require('moment');
const { randomString } = require('../utils');

class Member {

  // TODO constructor is not necessary.
  constructor(name = '', email = '') {
    this.id = '';
    this.name = name;
    this.email = email;
    this.password = '';
    this.salt = randomString(32);
    this.serverKey = randomString(32);
    this.deleted = false;
    this.createdTime = moment();
  }

  static parse(obj, id = '') {
    const user = new Member();
    user.id = id;
    user.name = obj.name ? obj.name : '';
    user.email = obj.email ? obj.email : '';
    user.password = obj.password ? obj.password : '';
    user.salt = obj.salt ? obj.salt : '';
    user.serverKey = obj.serverKey ? obj.serverKey : '';
    user.deleted = Boolean(obj.deleted);
    user.createdTime = obj.createdTime ? moment(obj.createdTime) : moment();

    return user;
  }

}

module.exports.Member = Member;
