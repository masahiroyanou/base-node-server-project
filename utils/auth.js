const crypto = require('crypto');

module.exports.hash = (password, salt) => {
  return crypto.pbkdf2Sync(password, salt, 10000, 32, 'sha512').toString('hex');
};
