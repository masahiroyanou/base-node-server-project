const randomstring = require('randomstring');

module.exports.randomString = number => {
  return randomstring.generate(number);
};
