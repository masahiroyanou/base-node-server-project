# Prepare
After clone, should you create `.env` file like below.

```
DATABASE_URL=postgres://postgres:******@localhost:5432/******
JWT_SECRET=**************
```

# Run
`node index.js`
