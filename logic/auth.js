const jwt = require('jsonwebtoken');
const moment = require('moment');
const { hash } = require('../utils');

class AuthLogic {

  constructor(service) {
    this.service = service;
  }

  async doLogin(email, password) {
    const user = await this.service.member.getMemberByEmail(email);
    if (!user) {
      return null;
    }

    if (user.password !== hash(password, user.salt)) {
      return null;
    }

    // TODO should search JWT
    const payload = {
      userId: user.id,
      exp: moment().add(30, 'd').unix(),
      iat: moment().unix(),
      iss: 'EDUN',
    };

    const token = jwt.sign(payload, process.env.JWT_SECRET + user.serverKey);
    return { token, userId: user.id };
  };

}

module.exports = AuthLogic;
