const { randomString } = require('../utils');

class BaseService {

  constructor(pool, table = '') {
    this.pool = pool;
    this.table = table;
  }

  async getCount(query, params) {
    let client = null;
    try {
      client = await this.pool.connect();
      const result = await client.query(query, params);

      return parseInt(result.rows[0].count, 10);
    } catch (e) {
      console.error(e);
      return 0;
    } finally {
      if (client) {
        client.release();
      }
    }
  }

  async getOneQuery(query, params) {
    let client = null;
    try {
      client = await this.pool.connect();
      const result = await client.query(query, params);

      return result.rows.length ? { id: result.rows[0].id, value: result.rows[0].value } : null;
    } catch (e) {
      console.error(e);
      return null;
    } finally {
      if (client) {
        client.release();
      }
    }
  }

  async getMultiQuery(query, params) {
    let client = null;
    try {
      client = await this.pool.connect();
      const result = await client.query(query, params);

      return result.rows.length ? result.rows : [];
    } catch (e) {
      console.error(e);
      return [];
    } finally {
      if (client) {
        client.release();
      }
    }
  }


  async getNewId(client) {
    const getQuery = `select * from ${this.table} where id = $1 and value->>'deleted' = 'false'`;

    let id, n = false;
    while (!n) {
      id = randomString(20);
      const result = await client.query(getQuery, [id]);
      n = !result.rows.length;
    }

    return id;
  }

  async insertQuery(obj) {
    let client = null;
    try {
      client = await this.pool.connect();

      const id = await this.getNewId(client);
      obj.id = id;

      await client.query('BEGIN');
      await client.query(`insert into ${this.table} (id, value) values($1, $2)`, [id, JSON.stringify(obj)]);
      await client.query('COMMIT');

      return id;
    } catch (e) {
      console.error(`failed to insert into ${this.table}`);
      console.warn(e);
      return '';
    } finally {
      if (client) {
        client.release();
      }
    }
  }

  async updateQuery(query, params) {
    let client = null;
    try {
      client = await this.pool.connect();

      await client.query('BEGIN');
      await client.query(query, params);
      await client.query('COMMIT');

      return '';
    } catch (e) {
      console.error(e);
      return '';
    } finally {
      if (client) {
        client.release();
      }
    }
  }

  async deleteQuery(query, params) {
    let client = null;
    try {
      client = await this.pool.connect();

      await client.query('BEGIN');
      await client.query(query, params);
      await client.query('COMMIT');

      return '';
    } catch (e) {
      console.error(e);
      return '';
    } finally {
      if (client) {
        client.release();
      }
    }
  }
}

module.exports = BaseService;
