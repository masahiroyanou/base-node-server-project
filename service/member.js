const BaseService = require('./base');
const { Member } = require('../objects');

class MemberService extends BaseService {

  constructor(pool) {
    super(pool, 'member');
  }

  async getMemberList() {
    const query = `select * from member where value->>'deleted' = 'false'`;

    const result = await super.getMultiQuery(query, []);
    return result ? result.map(row => Member.parse(row.value, row.id)) : null;
  }

  async getMemberListByIds(ids) {
    if (!ids.length) {
      return [];
    }

    const params = ids.map((id, index) => `$${index + 1}`);
    const query = `select * from member where id in (${params.join(',')}) and value->>'deleted' = 'false'`;

    const result = await super.getMultiQuery(query, ids);
    return result.map(row => Member.parse(row.value, row.id));
  }

  async getMemberById(memberId) {
    const query = `select * from member where id = $1 and value->>'deleted' = 'false'`;
    const params = [memberId];
    const result = await super.getOneQuery(query, params);

    return result ? Member.parse(result.value, result.id) : null;
  }

  async getMemberByEmail(email) {
    const query = `select * from member where value->>'email' = $1 and value->>'deleted' = 'false'`;
    const params = [email];
    const result = await super.getOneQuery(query, params);

    return result ? Member.parse(result.value, result.id) : null;
  }

  async createMember(member) {
    return await super.insertQuery(member);
  };

  async updateMember(member) {
    const query = `update member set value = $1 where id = $2`;
    const params = [JSON.stringify(member), member.id];
    return await super.updateQuery(query, params);
  }

}

module.exports = MemberService;
