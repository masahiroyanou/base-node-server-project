require('dotenv').config();
const url = require('url');
const express = require('express');
const bodyParser = require('body-parser');
const { Pool } = require('pg');

const { MemberService } = require('./service');
const { AuthLogic } = require('./logic');
const { AuthHandler } = require('./handler');

const port = process.env.PORT || 3335;

const databaseUrl = url.parse(process.env.DATABASE_URL);
const pool = new Pool({
  user: databaseUrl.auth.split(':')[0],
  host: databaseUrl.host.split(':')[0],
  database: databaseUrl.path.substring(1),
  password: databaseUrl.auth.split(':')[1],
  port: databaseUrl.port,
});

const service = {
  member: new MemberService(pool),
};

const logic = {
  auth: new AuthLogic(service),
};

const handler = {
  auth: new AuthHandler(logic),
};

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use((req, res, next) => {
  console.info(`[${req.method}] ${req.originalUrl}`);
  next();
});

/** API for login */
app.post('/api/login', handler.auth.doLogin());

app.listen(port, () => console.log(`Listening on ${port}`));
